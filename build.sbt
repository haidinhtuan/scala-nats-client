scalaVersion := "2.12.7"
version := "0.1"
name := "scala_nats_clients"

organization := "com.github.haidinhtuan"
description := "scala_nats_clients"

val scalaReflect = "org.scala-lang" % "scala-reflect" % "2.12.7"
val slf4jApi = "org.slf4j" % "slf4j-api" % "1.7.14"
val sl4fjSimple = "org.slf4j" % "slf4j-simple" % "1.7.5" % Test


lazy val root = (project in file(".")).
  settings(
    libraryDependencies ++= Seq(
      scalaReflect,
      slf4jApi,
      sl4fjSimple
    )
  )

